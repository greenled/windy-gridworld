package cu.uclv.vlir;

import javafx.fxml.FXMLLoader;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.Control;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;


public class ScreenManager extends StackPane {
    //Holds the screens to be displayed

    private HashMap<String, Node> screens = new HashMap<>();
    private Stage currentStage;

    public ScreenManager() {
        super();

        // loadScreen(SceneConfig.SPLASH_SCENE_ID, SceneConfig.SPLASH_SCENE_FILE);
        // loadScreen(SceneConfig.LOGIN_SCENE_ID, SceneConfig.LOGIN_SCENE_FILE);
    }

    public void close() {
        currentStage.close();

    }

    //Add the screen to the collection
    public void addScreen(String name, Node screen) {
        screens.put(name, screen);
    }

    //Returns the Node with the appropriate name
    public Node getScreen(String name) {
        return screens.get(name);
    }


    //Loads the fxml file, add the screen to the screens collection and
    //finally injects the screenPane to the controller.
    public boolean loadScreen(String name, String resource) {
        try {
            FXMLLoader myLoader = new FXMLLoader(getClass().getResource(resource));
            // myLoader.setResources(ATcneaSinglenton.getInstance().getI18N());
            Parent loadScreen = (Parent) myLoader.load();

            addScreen(name, loadScreen);
            return true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return false;
        }
    }

    public boolean loadScreen(String name, String resource, List<Object> objects) {
        try {
            FXMLLoader myLoader = new FXMLLoader(getClass().getResource(resource));
            // myLoader.setResources(ATcneaSinglenton.getInstance().getI18N());
            Parent loadScreen = (Parent) myLoader.load();

            return true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return false;
        }
    }

    public boolean loadScreen(String name, String resource, ControlledScreen controller) {
        try {
            Parent loadScreen = this.bindingFXML(controller, resource);

            return true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return false;
        }
    }

    public Parent bindingFXML(ControlledScreen controller, String filePath) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(Main.class.getResource(filePath));
            // fxmlLoader.setResources(ATcneaSinglenton.getInstance().getI18N());
            fxmlLoader.setController(controller);
            return fxmlLoader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public enum StageLayout {
        STANDARD, DIALOG
    }


    //This method tries to displayed the screen with a predefined name.
    //First it makes sure the screen has been already loaded.  Then if there is more than
    //one screen the new screen is been added second, and then the current screen is removed.
    // If there isn't any screen being displayed, the new screen is just added to the root.
    public boolean setScreen(final String name) {
        if (screens.get(name) != null) {   //screen loaded
            if (!getChildren().isEmpty()) {    //if there is more than one screen


                getChildren().remove(0);                    //remove the displayed screen
                getChildren().add(0, screens.get(name));     //add the screen

            } else {
                getChildren().add(screens.get(name));       //no one else been displayed, then just show
            }
            return true;
        } else {
            System.out.println("screen " + name + " hasn't been loaded!!! \n");
            return false;
        }
    }

    //This method will remove the screen with the given name from the collection of screens
    public boolean unloadScreen(String name) {
        if (screens.remove(name) == null) {
            System.out.println("Screen didn't exist");
            return false;
        } else {
            return true;
        }
    }

    /**
     * Function to load new AnchorPane content into other AnchorPane
     *
     * @param anchorPane
     * @param resource
     */
    public Parent loadAnchorPane(AnchorPane anchorPane, String resource) {
        return loadAnchorPane(anchorPane, resource, null);
    }

    /**
     * Function to load new AnchorPane content into other AnchorPane whit Controller screen
     *
     * @param anchorPane
     * @param resource
     * @param controlledScreen
     */
    public Parent loadAnchorPane(AnchorPane anchorPane, String resource, ControlledScreen controlledScreen) {
        Parent p = bindingFXML(controlledScreen, resource);
        AnchorPane.setTopAnchor(p, 0.0);
        AnchorPane.setRightAnchor(p, 0.0);
        AnchorPane.setLeftAnchor(p, 0.0);
        AnchorPane.setBottomAnchor(p, 0.0);
        anchorPane.getChildren().setAll(p);
        return p;
    }

    public static void openDialog(Alert.AlertType alertType, String s) {
        Alert alert = new Alert(alertType, s);
        alert.initStyle(StageStyle.UTILITY);
        alert.setTitle("ATcnea");
        alert.showAndWait();
    }


    public void openWindow(String fxml, String title, StageStyle style, ControlledScreen controlledScreen) {
        Stage stage = new Stage();
        stage.centerOnScreen();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.initStyle(StageStyle.UNDECORATED);
        stage.show();
    }


    public void hidenErrorTooltip(Control control) {
        if (control.getTooltip() != null) {
            control.getTooltip().hide();
            Tooltip.uninstall(control, control.getTooltip());
            control.getStyleClass().removeAll("errorTextField");
            control.getStylesheets().remove(getClass().getResource("/css/AlertStyles.css").toExternalForm());

        }
    }

    public void showErrorTooltip(Stage owner, Control control, String tooltipText) {
        Point2D p = control.localToScene(0.0, 0.0);
        Tooltip customTooltip;
        if (control.getTooltip() == null) {
            customTooltip = new Tooltip();
        } else {
            customTooltip = control.getTooltip();
        }
        customTooltip.setAutoHide(true);
        customTooltip.setText(tooltipText);
        customTooltip.setPrefWidth(control.getWidth());
        customTooltip.setMaxHeight(control.getWidth());
        customTooltip.setMinWidth(control.getWidth());
        customTooltip.setWrapText(true);
        if (!control.getStylesheets().contains(getClass().getResource("/css/AlertStyles.css").toExternalForm()))
            control.getStylesheets().add(getClass().getResource("/css/AlertStyles.css").toExternalForm());
        if (!control.getStyleClass().contains("errorTextField"))
            control.getStyleClass().add("errorTextField");
        control.setTooltip(customTooltip);
        customTooltip.setStyle("-fx-text-fill: rgba(255, 27, 13, 0.95);-fx-background-color: rgba(255, 120, 93, 1);-fx-background-insets: 0;-fx-background-radius: 0 0 0 0;");
        customTooltip.show(owner, p.getX() + control.getScene().getX() + control.getScene().getWindow().getX(), p.getY() + control.getScene().getY() + control.getScene().getWindow().getY() + control.getHeight());
    }

    public void setStage(Stage primaryStage) {
        this.currentStage = primaryStage;
    }

    public Stage getStage() {
        return this.currentStage;
    }

}

