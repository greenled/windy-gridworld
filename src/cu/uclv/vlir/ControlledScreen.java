
package cu.uclv.vlir;


import javafx.scene.layout.AnchorPane;

import java.util.List;

public interface ControlledScreen {

    //This method will allow the injection of the Parent ScreenPane
    public void setScreenParent(ScreenManager screenPage);

    public void initData(List<Object> objectList);

    //Alert Pane
    public AnchorPane getAlertTopPane();
}
