package cu.uclv.vlir;

import java.util.ArrayList;
import java.util.List;

/**
 * A cell of the grid world
 */
public class Cell {

    /**
     * North action
     */
    private Action north = new Action("N", -1, 0);

    /**
     * Northeast action
     */
    private Action northeast = new Action("NE", -1, 0);

    /**
     * East action
     */
    private Action east = new Action("E", -1, 0);

    /**
     * Southeast action
     */
    private Action southeast = new Action("SE", -1, 0);

    /**
     * South action
     */
    private Action south = new Action("S", -1, 0);

    /**
     * Southwest action
     */
    private Action southwest = new Action("SW", -1, 0);

    /**
     * West action
     */
    private Action west = new Action("W", -1, 0);

    /**
     * Northwest action
     */
    private Action northwest = new Action("NW", -1, 0);

    /**
     * Column index
     */
    private int column;

    /**
     * Row index
     */
    private int row;

    public Cell (int column, int row) {
        this.column = column;
        this.row = row;
    }

    public Action getNorth() {
        return north;
    }

    public Action getNortheast() {
        return northeast;
    }

    public Action getEast() {
        return east;
    }

    public Action getSoutheast() {
        return southeast;
    }

    public Action getSouth() {
        return south;
    }

    public Action getSouthwest() {
        return southwest;
    }

    public Action getWest() {
        return west;
    }

    public Action getNorthwest() {
        return northwest;
    }

    public int getColumn() {
        return column;
    }

    public int getRow() {
        return row;
    }

    /**
     * Get an array with all actions
     * @return actions
     */
    public Action[] getActions () {
        Action[] actions = {north, northeast, east, southeast, south, southwest, west, northwest};
        return actions;
    }

    /**
     * Get an array with the bets actions (the ones with the highest Q(s,a) value)
     * @return best actions
     */
    public Action[] getBestActions () {
        Action[] actions = getActions();
        Action firstBestAction = getFirstBestAction();
        List<Action> bestActionsList = new ArrayList<>();
        bestActionsList.add(firstBestAction);
        for (int i = 1; i < actions.length; i++) {
            if (actions[i].getqValue() == firstBestAction.getqValue() && !actions[i].equals(firstBestAction)) {
                bestActionsList.add(actions[i]);
            }
        }
        Action[] bestActions = new Action[bestActionsList.size()];
        bestActionsList.toArray(bestActions);
        return bestActions;
    }

    /**
     * Get the first best action (any of the best actions)
     * @return first best action
     */
    public Action getFirstBestAction () {
        Action[] actions = getActions();
        Action firstBestAction = actions[0];
        for (int i = 1; i < actions.length; i++) {
            if (actions[i].getqValue() > firstBestAction.getqValue()) {
                firstBestAction = actions[i];
            }
        }
        return firstBestAction;
    }

    public String toString () {
        return "cell ("+column+";"+row+")";
    }

}

