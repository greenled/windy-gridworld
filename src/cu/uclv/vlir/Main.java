package cu.uclv.vlir;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    public static ScreenManager screenManager;

    public static void main(String[] args) {
        screenManager = new ScreenManager();
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        stage.setTitle("Windy GridWorld");
        stage.sizeToScene();
        stage.setResizable(false);
        screenManager.loadScreen("start_scene", "main.fxml");
        screenManager.setScreen("start_scene");
        screenManager.setStage(stage);
        stage.centerOnScreen();

        Group root = new Group();
        root.getChildren().addAll(screenManager);
        Scene scene = new Scene(root);

        stage.setScene(scene);
        stage.show();
    }
}
