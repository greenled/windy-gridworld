package cu.uclv.vlir;

/**
 * A possible action that can be taken by the agent in the grid
 */
public class Action {

    /**
     * Action name
     * eg. North West
     */
    private String name;

    /**
     * R value for Q(s,a)
     */
    private double rValue;

    /**
     * Q (s,a)
     */
    private double qValue;

    public Action(String name, double rValue, double qValue) {
        this.name = name;
        this.rValue = rValue;
        this.qValue = qValue;
    }

    public String getName() {
        return name;
    }

    public double getrValue() {
        return rValue;
    }

    public double getqValue() {
        return qValue;
    }

    public void setqValue(double qValue) {
        this.qValue = qValue;
    }

    public String toString () {
        return "action ("+name+")";
    }

}
