/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cu.uclv.vlir;

import java.util.List;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.stage.Stage;
import cu.uclv.vlir.Agent.*;

/**
 *
 * @author Grethica
 */
public class Graph {
    public static void plotRewards(List<ExplorationReport> report){
        Stage stage = new Stage();
        stage.setTitle("Windy GridWorld");
        final NumberAxis xAxis = new NumberAxis();
        final NumberAxis yAxis = new NumberAxis();
        xAxis.setLabel("Episodes");
        yAxis.setLabel("Rewards");
        
        final LineChart<Number,Number> lineChart = new LineChart<Number,Number>(xAxis,yAxis);                
        lineChart.setTitle("Total Collected Reward Versus Episodes");                                
        lineChart.setCreateSymbols(false);
        XYChart.Series series = new XYChart.Series();
        series.setName("Rewards collected");
        
        for (int i = 0; i < report.size(); i+=25){
            double rewards = report.get(i).getCollectedReward();
            series.getData().add(new XYChart.Data(i, rewards/(i+1)));
        }        
        
        Scene scene  = new Scene(lineChart,800,600);
        lineChart.getData().add(series);
       
        stage.setScene(scene);
        stage.show();
    }
    
    public static void plotSteps (List<ExplorationReport> report){
        Stage stage = new Stage();
        stage.setTitle("Windy GridWorld");
        final NumberAxis xAxis = new NumberAxis();
        final NumberAxis yAxis = new NumberAxis();
        xAxis.setLabel("Episodes");
        yAxis.setLabel("Number of steps");
        
        final LineChart<Number,Number> lineChart = new LineChart<Number,Number>(xAxis,yAxis);                
        lineChart.setTitle("Steps To Reach The Goal Versus Episodes");                                
        lineChart.setCreateSymbols(false);
        XYChart.Series series = new XYChart.Series();
        series.setName("Number of steps");
        
        for (int i = 0; i < report.size(); i+=25){
            series.getData().add(new XYChart.Data(i, report.get(i).getSteps()));
        }     
        
        Scene scene  = new Scene(lineChart,800,600);
        lineChart.getData().add(series);
       
        stage.setScene(scene);
        stage.show();
    }
}
