package cu.uclv.vlir;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.scene.text.Font;
import cu.uclv.vlir.Agent.*;
import java.util.ArrayList;

public class Controller extends AnchorPane implements Initializable {

    @FXML
    private GridPane board;

    @FXML
    private TextField epsilonTF;

    @FXML
    private TextField taoTF;

    @FXML
    private TextField alphaTF;
    
    @FXML
    private TextField episodes;

    private int[] wind = {0, 0, 1, 1, 1, 2, 2, 1, 0, 0, 0, 0};
    private Grid grid;
    private Agent agent;
    private Cell s;
    private Cell g;
    List<Cell> walk;
    List<ExplorationReport> report;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        Platform.runLater(() -> {
            run();
            fillGridPane();
        });
    }

    public void run() {
        grid = new Grid(12, 7, wind);
        agent = new Agent();
        s = grid.getCell(0, 3);
        g = grid.getCell(9, 3);
        report = new ArrayList<>();
        int iterations = new Integer(episodes.getText());
        for (int i = 0; i < iterations; i++) {
            report.add(agent.exploreGrid(grid, s, g, new Double(alphaTF.getText()), new Double(taoTF.getText()), new Double(epsilonTF.getText())));
        }
    }

    public void fillGridPane() {

        int rowSize = board.getRowConstraints().size();
        int colSize = board.getColumnConstraints().size();

        for (int row = 0; row < rowSize; row++) {
            for (int col = 0; col < colSize; col++) {
                StackPane square = new StackPane();

                //init cell style
                if (col == 0 && row == 3) {
                    square.setStyle("-fx-background-color: #E9B96E; -fx-border-color: gray; -fx-border-width: 1px;");
                }

                //end cell style
                if (col == 9 && row == 3) {
                    Text goalT = new Text("G");
                    square.getChildren().add(goalT);
                    square.setStyle("-fx-background-color: #aff8ad; -fx-border-color: gray; -fx-border-width: 1px;");
                }
                board.add(square, col, row);
            }
        }

        for (int columns = 0; columns < grid.getColumnsCount(); columns++) {
            for (int rows = 0; rows < grid.getRowsCount(); rows++) {
                if (columns == 9 && rows == 3) {
                    continue;
                }
                Cell cell = grid.getCell(columns, rows);
                addMoveDirection(cell.getBestActions(), cell.getRow(), cell.getColumn(), Color.BLACK);
            }
        }
    }

    public void walk(ActionEvent e) {
        cleanSimulate();
        walk = agent.walkGrid(grid, grid.getCell(0, 3), grid.getCell(9, 3)).getCells();
        for (int i = 0; i < walk.size(); i++) {
            Cell cw = walk.get(i);
            if (i == 0 || i == walk.size() - 1) {
                continue;
            }
            drawBestWay(cw.getRow(), cw.getColumn());
            addMoveDirection(cw.getBestActions(), cw.getRow(), cw.getColumn(), Color.BLACK);
        }
    }

    public void addMoveDirection(Action[] actions, int row, int col, Color color) {
        String text = "";
        for (int i = 0; i < actions.length; i++) {
            text += actions[i].getName();
            if (i != actions.length - 1) {
                text += "-";
            }

            if (i != 0 && i % 2 != 0) {
                text += "\n";
            }
        }

        Text directionMove = new Text(text);
        directionMove.setFill(color);
        directionMove.setFont(Font.font("Verdana", 10));
        board.add(directionMove, col, row);
    }

    public void drawBestWay(int row, int col) {
        StackPane square = new StackPane();
        square.setStyle("-fx-background-color: #aff8ad; -fx-border-color: gray; -fx-border-width: 1px;");
        board.add(square, col, row);
    }

    public void cleanSimulate() {
        if (walk != null) {
            for (int i = 0; i < walk.size(); i++) {
                Cell cw = walk.get(i);
                if (i == 0 || i == walk.size() - 1) {
                    continue;
                }
                StackPane square = new StackPane();
                square.setStyle("-fx-background-color: #fff; -fx-border-color: gray; -fx-border-width: 1px;");
                board.add(square, cw.getColumn(), cw.getRow());
                addMoveDirection(cw.getBestActions(), cw.getRow(), cw.getColumn(), Color.BLACK);
            }
        }
    }
    
    public void cleanGridPane(){
        for (int i = 0; i < grid.getColumnsCount(); i++) {
            for (int j = 0; j < grid.getRowsCount(); j++) {
                StackPane square = new StackPane();
                square.setStyle("-fx-background-color: #fff; -fx-border-color: gray; -fx-border-width: 1px;");
                board.add(square, i, j);                
            }
        }
    }

    public void runTrain(ActionEvent e){
        run();
        cleanGridPane();
        fillGridPane();        
    }

    public void plotRewards(ActionEvent e){
        Graph.plotRewards(report);
    }

    public void plotSteps (ActionEvent e){
        Graph.plotSteps(report);
    }
}
