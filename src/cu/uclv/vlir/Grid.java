package cu.uclv.vlir;

/**
 * The grid world
 */
public class Grid {
    /**
     * Grid cells as a Cell matrix
     */
    private Cell[][] cells;

    /**
     * Wind's mean strengths for each column as a vector
     */
    private int[] wind;

    public Grid (int cellsH, int cellsV, int[] wind) {
        cells = new Cell[cellsH][cellsV];
        this.wind = wind;

        // Create cells
        for (int h = 0; h < cellsH; h++) {
            for (int v = 0; v < cellsV; v++) {
                cells[h][v] = new Cell(h, v);
            }
        }
    }

    public Cell getCell(int column, int row) {
        return cells[column][row];
    }

    /**
     * Get the number of rows
     * @return number of rows
     */
    public int getRowsCount () {
        return cells[0].length;
    }

    /**
     * Get the number of columns
     * @return number of columns
     */
    public int getColumnsCount () {
        return cells.length;
    }

    /**
     * Apply the wind force to a given position
     * @param cell the position
     * @return resultant position
     */
    public Cell applyWind (Cell cell) {
        if (wind[cell.getColumn()] == 0) {
            return cell;
        } else {
            int displacement = wind[cell.getColumn()] - 1 + ((int) Math.floor(Math.random() * (3)));
            int newRow = cell.getRow() - displacement;
            if (newRow < 0) newRow = 0;
            return cells[cell.getColumn()][newRow];
        }
    }

    public int[] getWind() {
        return wind;
    }

}
