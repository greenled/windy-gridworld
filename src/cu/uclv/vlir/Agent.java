package cu.uclv.vlir;

import java.util.ArrayList;
import java.util.List;

/**
 * An agent moving through a grid world
 */
public class Agent {

    /**
     * Explore a grid
     * @param grid
     * @param s
     * @param g
     * @param alpha
     * @param tao
     */
    public ExplorationReport exploreGrid (Grid grid, Cell s, Cell g, double alpha, double tao, double epsilon) {
        ExplorationReport report = new ExplorationReport();
        do {
            Action a = selectActionGreedy(s.getActions(), epsilon);
            Cell nextS = selectNextCell (grid, s, a);
            a.setqValue(a.getqValue() + alpha * (a.getrValue() + tao * (selectMaxQ(nextS)) - a.getqValue()));
            s = nextS;
            report.steps++;
            report.collectedReward += a.getqValue();
        } while (!s.equals(g));
        return report;
    }

    /**
     * Walk a grid
     * Should explore it first
     * @param grid
     * @param s
     * @param g
     * @return
     */
    public WalkReport walkGrid (Grid grid, Cell s, Cell g) {
        WalkReport report = new WalkReport();
        report.cells.add(s);
        do {
            Action a = selectActionGreedy(s.getBestActions());
            s = selectNextCell (grid, s, a);
            report.cells.add(s);
        } while (!s.equals(g));
        return report;
    }

    /**
     * Report given by the agent after walking a grid
     */
    public static class WalkReport {
        private List<Cell> cells = new ArrayList<>();

        public List<Cell> getCells() {
            return cells;
        }
    }

    /**
     * Report given by the agent after exploring a grid
     */
    public static class ExplorationReport {
        private double collectedReward = 0;
        private int steps = 0;

        public double getCollectedReward() {
            return collectedReward;
        }

        public int getSteps() {
            return steps;
        }
    }

    /**
     * Select an action with an epsilon greedy strategy
     * @param actions
     * @param epsilon
     * @return an action
     */
    private Action selectActionGreedy (Action[] actions, double epsilon) {
        double r = Math.random();
        if(r < epsilon){
            return selectActionRandom(actions);
        } else {
            return selectActionGreedy(actions);
        }
    }

    /**
     * Select an action with a random strategy
     * @param actions
     * @return an action
     */
    private Action selectActionRandom (Action[] actions) {
        return actions[(int) Math.floor(Math.random() * actions.length)];
    }

    /**
     * Select an action with a greedy strategy
     * @param actions
     * @return an action
     */
    private Action selectActionGreedy(Action[] actions){
        Action action = actions[0];
        for(int i = 1; i < actions.length; i++){
            if(actions[i].getqValue() > action.getqValue()) {
                action = actions[i];
            }
        }
        return action;
    }

    /**
     * Select the next cell to move to
     * @param grid
     * @param s
     * @param a
     * @return the selected cell
     */
    private Cell selectNextCell (Grid grid, Cell s, Action a) {
        if (a.equals(s.getNorth())) {
            if (s.getRow() > 0) {
                return grid.applyWind(grid.getCell(s.getColumn(), s.getRow() - 1));
            } else {
                return s;
            }
        } else if (a.equals(s.getSouth())) {
            if (s.getRow() < grid.getRowsCount() - 1) {
                return grid.applyWind(grid.getCell(s.getColumn(), s.getRow() + 1));
            } else {
                return s;
            }
        } else if (a.equals(s.getWest())) {
            if (s.getColumn() > 0) {
                return grid.applyWind(grid.getCell(s.getColumn() - 1, s.getRow()));
            } else {
                return s;
            }
        } else if (a.equals(s.getEast())) {
            if (s.getColumn() < grid.getColumnsCount() - 1) {
                return grid.applyWind(grid.getCell(s.getColumn() + 1, s.getRow()));
            } else {
                return s;
            }
        } else if (a.equals(s.getNorthwest())) {
            if (s.getRow() > 0 && s.getColumn() > 0) {
                return grid.applyWind(grid.getCell(s.getColumn() - 1, s.getRow() - 1));
            } else {
                return s;
            }
        } else if (a.equals(s.getNortheast())) {
            if (s.getRow() > 0 && s.getColumn() < grid.getColumnsCount() - 1) {
                return grid.applyWind(grid.getCell(s.getColumn() + 1, s.getRow() - 1));
            } else {
                return s;
            }
        } else if (a.equals(s.getSouthwest())) {
            if (s.getRow() < grid.getRowsCount() - 1 && s.getColumn() > 0) {
                return grid.applyWind(grid.getCell(s.getColumn() - 1, s.getRow() + 1));
            } else {
                return s;
            }
        // In case a.equals(s.getSoutheast())
        } else {
            if (s.getRow() < grid.getRowsCount() - 1 && s.getColumn() < grid.getColumnsCount() - 1) {
                return grid.applyWind(grid.getCell(s.getColumn() + 1, s.getRow() + 1));
            } else {
                return s;
            }
        }
    }

    /**
     * Select the maximum Q value for a cell actions
     * @param s
     * @return maximum Q value
     */
    private double selectMaxQ (Cell s) {
        Action[] actions = s.getActions();
        double maxQ = actions[0].getqValue();
        for (int i = 1; i < actions.length; i++) {
            if (actions[i].getqValue() > maxQ) {
                maxQ = actions[i].getqValue();
            }
        }
        return maxQ;
    }

}
